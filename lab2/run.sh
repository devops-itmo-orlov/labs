clear

docker run -i --rm -t -v "/$(pwd)/index.html:/usr/share/nginx/html/index.html" \
                 -v "/$(pwd)/nginx/conf.d:/etc/nginx/conf.d" \
                 --name nginx-lab-2 -p 81:81 nginx:mainline-alpine
