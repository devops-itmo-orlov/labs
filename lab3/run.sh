#!/bin/sh

clear

export $(grep -v '^#' .env | xargs)

# docker build -t docker-app-lab-3:latest .

# docker run -i --rm -t -v "/$(pwd):/app" -p $PORT:$PORT --env-file .env docker-app-lab-3:latest

docker-compose up --build
