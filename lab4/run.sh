#!/bin/sh

clear

export $(grep -v '^#' .env | xargs)

docker build -t multistaging-lab-4:latest .

docker run -i --rm -t -p $PORT:$PORT --env-file .env multistaging-lab-4:latest
