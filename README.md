# Labs

## Lab 2 - Simple nginx server
To execute use: 
- ```cd lab2```
- ```docker run -i --rm -t -v "/$(pwd)/index.html:/usr/share/nginx/html/index.html" -v "/$(pwd)/nginx/conf.d:/etc/nginx/conf.d" --name nginx-lab-2 -p 81:81 nginx:mainline-alpine```
- go to [localhost:81](localhost:81)

## Lab 3 - Simple flask docker app
To execute use:
- ```cd lab3```
- ```. run.sh```
- go to [localhost:82](localhost:82)

## Lab 4 - Simple ASPNET multistaging app
To execute use:
- ```cd lab4```
- ```. run.sh```
- go to [localhost:83](localhost:83)

## Lab 5 and 6 - React + python app
To execute use:
- ```cd lab5_6```
- ```docker-compose up --build -d```
- go to [localhost:1234](localhost:1234)
